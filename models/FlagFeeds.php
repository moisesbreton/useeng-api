<?php
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class FlagFeeds extends Model {

    public function initialize() {
        $this->hasOne("user_id", "Users", "id");
        $this->hasOne("feed_id", "Feeds", "id");
    }

    public function onConstruct() {
        $this->date_created = date("Y/m/d H:i:s");
    }

    public function afterCreate() {
        $this->refresh();
    }

    public function validation() {
        //Feeds id must to be unique
        $this->validate(new Uniqueness(
            [
                "field" => "id",
                "message" => "The feed id must be unique",
            ]
        ));

        //Check if any messages have been produced
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

}