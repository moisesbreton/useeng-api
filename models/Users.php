<?php
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Users extends Model {

	public function onConstruct() {
		$this->has_photo = 0;
		$this->date_created = date("Y/m/d H:i:s");
		$this->date_updated = date("Y/m/d H:i:s");
	}

	public function afterSave() {
		$this->refresh();
	}

	public function validation() {
		//User name must be unique
		$this->validate(new Uniqueness(
			[
				"field" => "email",
				"message" => "The user name must be unique",
			]
		));

		//Check if any messages have been produced
		if ($this->validationHasFailed() == true) {
			return false;
		}
	}

	public function getUserByUserName($user_name) {
		$params = [
			'conditions' => 'name = :user_name:',
			'bind' => ['user_name' => $user_name],
		];

		$user = Users::find($params)->getFirst();

		return $user;
	}

	public function getProfilePicturePath($config) {
		$media_url = $config->media["media_url"];
		$profile_path_placeholder = $media_url . $config->images['profile_path_placeholder'];
		$ret = $profile_path_placeholder;
		if ($this->has_photo && !empty($this->upload_path) && !empty($config)) {
			$p_date = strtotime($this->date_updated);
			$timestamp = date('YmdHis', $p_date);
			$filename = $config->media_name['profile_image'] . $config->media_type['image'];
			$ret = $media_url . $config->images['profile_path'] . "{$this->upload_path}/{$filename}?t={$timestamp}";
		}
		return $ret;
	}

}
