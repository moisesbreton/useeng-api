<?php
use Phalcon\Mvc\Model;

class Categories extends Model {

	public function onConstruct() {
		//Default values
	}

	public function afterSave() {
		$this->refresh();
	}

	public function validation() {
		//Check if any messages have been produced
		if ($this->validationHasFailed() == true) {
			return false;
		}
	}

}