<?php
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Feeds extends Model {

	public function initialize() {
		$this->hasOne("user_id", "Users", "id");
		$this->hasOne("category_id", "Categories", "id");
	}

	public function onConstruct() {
		$this->date_created = date("Y/m/d H:i:s");
		$this->status = 1;
	}

	public function afterCreate() {
		$this->refresh();
	}

	public function validation() {
		//Feeds id must to be unique
		$this->validate(new Uniqueness(
			[
				"field" => "id",
				"message" => "The feed id must be unique",
			]
		));

		//Check if any messages have been produced
		if ($this->validationHasFailed() == true) {
			return false;
		}
	}

	public function getFeedPicturePath($config) {
		$ret = "";
		if (!empty($this->upload_path) && !empty($config)) {
			$p_date = strtotime($this->date_created);
			$timestamp = date('YmdHis', $p_date);
			$media_url = $config->media["media_url"];
			$video_feed_image_name = $config->media_name["feed_image"] . $config->media_type['image'];
			$ret = $media_url . $config->images['feed_path'] . "{$this->upload_path}/{$video_feed_image_name}?t={$timestamp}";
		}
		return $ret;
	}

	public function getFeedVideoSource($config) {
		$ret = "";
		if (!empty($this->upload_path) && !empty($config)) {
			$p_date = strtotime($this->date_created);
			$timestamp = date('YmdHis', $p_date);
			$media_url = $config->media["media_url"];
			$video_feed_path = $config->videos['feed_path'] . "{$this->upload_path}/";
			$ret = $media_url . $video_feed_path . $config->media_name["feed_video"] . $config->media_type['video'] . "?t={$timestamp}";
		}
		return $ret;
	}

}