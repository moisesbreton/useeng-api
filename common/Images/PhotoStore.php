<?php
namespace Useeng\Common\Images\PhotoStore;

use Aws\S3\S3Client;
use Phalcon\Config\Adapter\Ini as IniConfig;

include_once USEENG_ROOT . "aws/vendor/autoload.php";

class Images {
	private $image = null;
	private $image_type;
	private $_image_sizes = [
		'profile' => [
			'width' => 110,
			'height' => 110,
			'crop' => false,
			'quality' => 85,
		],
		'feed' => [
			'width' => 640,
			'height' => 640,
			'crop' => false,
			'quality' => 85,
		],
		'default' => [
			'width' => 205,
			'height' => 205,
			'crop' => false,
			'quality' => 85,
		],
	];

	protected static $_static_init = false;
	protected static $_server;
	protected static $_database;
	protected static $_base_url;
	protected static $_size_to_folder_path = array(
		'profile' => 'images/profile',
		'feed' => 'feeds/images',
		'videos' => 'feeds/videos',
	);

	protected static $_config;

	protected $_log_to_file = true;
	protected $_s3_client;

	function __construct($image_stream = null) {

		$this->_s3_client = S3Client::factory([
			'key' => 'AKIAJIN74IF73W2GNZYA',
			'secret' => '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',
		]);

		$this->_config = new IniConfig(USEENG_ROOT . "config.ini.php");

		if ($image_stream) {
			$this->image = imagecreatefromstring($image_stream);
		} else {
			$this->image = null;
		}
	}

	function __get($varname) {
		switch ($varname) {
			case 'width':return imagesx($this->image);
				break;
			case 'height':return imagesy($this->image);
				break;
		}
	}

	function __destruct() {
		if ($this->image) {
			imagedestroy($this->image);
		}

	}

	function create($width, $height, $fill_color = null) {
		$this->image = imagecreatetruecolor($width, $height);
		if ($fill_color !== null) {
			$this->fill($fill_color);
		}

		return $this;
	}

	function fill($fill_color) {
		imagefill($this->image, 0, 0, $fill_color);
		return $this;
	}

	function load($filename) {
		$image_info = getimagesize($filename);

		$this->image_type = $image_info[2];
		if ($this->image_type == IMAGETYPE_JPEG) {
			$this->image = imagecreatefromjpeg($filename);
		} elseif ($this->image_type == IMAGETYPE_GIF) {
			$this->image = imagecreatefromgif($filename);
		} elseif ($this->image_type == IMAGETYPE_PNG) {
			$this->image = imagecreatefrompng($filename);
		} else {
			//$this->image = imagecreatefromwbmp($filename);
			$this->image = imagecreatefrombmp($filename); //#5719
		}
		return $this;
	}

	function save($id, $image_type = IMAGETYPE_JPEG, $size = "default", $quality = 70) {

		$filename = $this->_config->media_name["{$size}_image"] . $this->_config->media_type["image"];
		$path = $this->_config->images['temp_path'] . $this->_config->images["{$size}_path"] . "{$id}/";

		if (!is_dir($path)) {
			mkdir("{$path}", 0777, true);
		}

		$filename = $path . $filename;

		$rules = isset($this->_image_rules[$size])
		? $this->_image_rules[$size]
		: $this->_image_rules['default'];

		try {
			ob_start();
			if ($image_type == IMAGETYPE_JPEG) {
				\imagejpeg($this->image, $filename, $quality);
			} elseif ($image_type == IMAGETYPE_GIF) {
				\imagegif($this->image, $filename);
			} elseif ($image_type == IMAGETYPE_PNG) {
				\imagepng($this->image, $filename);
			}

			ob_get_clean();

			return $this->store($id, $filename, $size);

		} catch (Exception $e) {
			return false;
		}
	}

	function saveJPG($id, $size, $quality) {
		//imagejpeg($this->image, $filename, $quality );
		return $this->save($id, IMAGETYPE_JPEG, $size, $quality);
	}

	function savePNG($id, $size) {
		return $this->save($id, IMAGETYPE_PNG, $size);
	}

	function output($image_type = IMAGETYPE_JPEG) {
		if ($image_type == IMAGETYPE_JPEG) {
			imagejpeg($this->image);
		} elseif ($image_type == IMAGETYPE_GIF) {
			imagegif($this->image);
		} elseif ($image_type == IMAGETYPE_PNG) {
			imagepng($this->image);
		}
	}

	function getImageBits($image_type = IMAGETYPE_JPEG) {
		ob_start();
		if ($image_type == IMAGETYPE_JPEG) {
			imagejpeg($this->image);
		} elseif ($image_type == IMAGETYPE_GIF) {
			imagegif($this->image);
		} elseif ($image_type == IMAGETYPE_PNG) {
			imagepng($this->image);
		}
		$i = ob_get_clean();
		return $i;
	}

	function resizeToHeight($height) {
		$ratio = $height / $this->height;
		$width = $this->width * $ratio;
		$this->resize($width, $height);
		return $this;
	}

	function resizeToWidth($width) {
		$ratio = $width / $this->width;
		$height = $this->height * $ratio;
		$this->resize($width, $height);
		return $this;
	}

	function scale($scale) {
		$width = $this->width * $scale / 100;
		$height = $this->height * $scale / 100;
		$this->resize($width, $height);
		return $this;
	}

	function resize($width, $height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresized($new_image, $this->image, 0, 0, 0, 0,
			$width, $height, $this->width, $this->height);
		imagedestroy($this->image);
		unset($this->image);
		$this->image = $new_image;
		return $this;
	}

	function crop($x0, $y0, $width, $height) {
		$new_image = imagecreatetruecolor($width, $height);
		imagecopy($new_image, $this->image, 0, 0, $x0, $y0, $width, $height);
		imagedestroy($this->image);
		unset($this->image);
		$this->image = $new_image;
		return $this;
	}

	function copymerge($other_image, $x, $y, $alpha = 100) {
		imagecopymerge($this->image,
			$other_image->image,
			$x, $y,
			0, 0,
			$other_image->width, $other_image->height,
			$alpha);
		return $this;
	}

	function copy($other_image, $x, $y, $alpha = 100) {
		imagecopy($this->image,
			$other_image->image,
			$x, $y,
			0, 0,
			$other_image->width, $other_image->height);
		return $this;
	}

	function setBackgroundColor($r, $g, $b) {
		$this->bg_color = imagecolorallocate($this->image, $r, $g, $b);
		$this->fill($this->bg_color);
		return $this;
	}

	function setForegroundColor($r, $g, $b) {
		$this->fg_color = imagecolorallocate($this->image, $r, $g, $b);
		return $this;
	}

	function setFont($font) {
		$this->font = $font;
		return $this;
	}

	function drawText($text, $x, $y, $size) {
		imagettftext($this->image, $size, 0, $x, $y, $this->fg_color, $this->font, $text);
		return $this;
	}

	protected function resizeWithAlpha($width, $height) {
		$width_old = $this->width;
		$height_old = $this->height;

		//# This is the resizing/resampling/transparency-preserving
		$image_resized = imagecreatetruecolor($width, $height);

		$trnprt_indx = imagecolortransparent($this->image);

		imagealphablending($image_resized, false);
		//$color = imagecolorallocatealpha( $image_resized, 0, 0, 0, 127 );
		//imagefill( $image_resized, 0, 0, $color );
		imagesavealpha($image_resized, true);

		imagecopyresampled($image_resized, $this->image, 0, 0, 0, 0, $width,
			$height, $width_old, $height_old);

		imagedestroy($this->image);
		unset($this->image);
		$this->image = $image_resized;
		return $this;
	}

	function applyWatermark($info) {
		//return $this->_ApplyWatermark( $info->path, 'small' );
		$wm = new SimpleImage();

		$wm->load($info->path);

		$new_width = $wm->width * $info->scale / 100.0;
		$new_height = $wm->height * $info->scale / 100.0;

		$x_pos = $this->width * ($info->pct_x / 100.0) - $new_width * ($info->pct_x / 100.0);
		$y_pos = $this->height * ($info->pct_y / 100.0) - $new_height * ($info->pct_y / 100.0);

		$wm->resizeWithAlpha($new_width, $new_height);

		$this->copy($wm, $x_pos, $y_pos);

		return $this;
	}

	public function store($id, $pathToFile, $size) {
		if ($id && $pathToFile && $size) {

			$media = "useeng-static"; //self::getMedia($section, $params);
			$upload_path = $this->generateUploadPath($id);
			$uri = $this->_getAssetPath($upload_path, $size);
			$ret = null;

			if (!empty($media) && !empty($uri)) {
				$ret = $this->_s3_client->putObject(array(
					'Bucket' => $media,
					'Key' => $uri,
					'Body' => fopen($pathToFile, 'r+'),
				));
			} else {
				return false;
			}

			$this->rrmdir($pathToFile);

			return $ret != null ? $upload_path : false;
		} else {
			$this->rrmdir($pathToFile);
			return false;
		}
	}

	public function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);

			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir . "/" . $object) == "dir") {
						rrmdir($dir . "/" . $object);
					} else {
						unlink($dir . "/" . $object);
					}

				}
			}

			reset($objects);
			rmdir($dir);
		}
	}

	protected $_photo_structure_created = array();

	protected function _createPhotoStructure($path/*, $mode = 0777, $modify = false*/) {
		if (!isset($this->_photo_structure_created[$path])) {
			clearstatcache();

			$files = glob("{$path}/small");

			if (count($files) == 0) {

				$mode = 0777;

				$saved_umask = umask(0);

				mkdir("{$path}/small", $mode, true);
				mkdir("{$path}/medium", $mode);
				mkdir("{$path}/large", $mode);
				mkdir("{$path}/vip", $mode);

				mkdir("{$path}/attachments", $mode);
				//mkdir("{$path}/modify", $mode);
				mkdir("{$path}/modify/small", $mode, true);
				mkdir("{$path}/modify/medium", $mode);
				mkdir("{$path}/modify/large", $mode);
				mkdir("{$path}/modify/vip", $mode);

				if (!is_dir($path)) {
					$this->log("Could not create photo directory {$path}");
				}

				umask($saved_umask);

				//$this->log("createPhotoStructure('$path')");

				$this->_photo_structure_created[$path] = true;
			}
		}
	}

	protected function generateUploadPath($id, $hash = null) {
		$dir = md5($id);

		//#6992
		if (!empty($hash)) {
			$id += "_" . $hash;
		}

		return $dir{0} . $dir{1} . '/' . $dir{2} . '/' . $id;
	}

	protected function _getAssetUrl($section, $params) {
		if (!empty($section) && !empty($params)) {
			if ($params['name']) {
				$params['name'] = rawurlencode($params['name']);
			}
			$url = $this->_base_url . '/' . $this->_getAssetPath($section, $params);
		} else {
			$url = $this->_base_url;
		}

		return $url;
	}

	protected function _getAssetPath($upload_path, $size) {
		$folder_path = $this->_config->images[$size . '_path'];
		$filename = $this->_config->media_name[$size . '_image'] . $this->_config->media_type['image'];

		$ret = sprintf('%s/%s/%s', $folder_path, $upload_path, $filename);

		// Prevent double slashes as some asset stores (like S3) will try to create a blank folder.
		// Ex: some/path//to/file should become some/path/to/file
		$ret = str_replace('//', '/', $ret);

		return $ret;
	}

	protected function log() {
		$args = func_get_args();
		$format = array_shift($args);
		$this->imageLog($format, $args);
	}

	public function imageLog($format, $args) {
		$logmsg = sprintf("[%s] %s\n", gmstrftime('%Y-%m-%d %H:%M:%S'), vsprintf($format, $args));

		if ($this->_log_to_file) {
			file_put_contents('/tmp/imagemanager.log', $logmsg, FILE_APPEND);
		}

		$this->log($logmsg);
	}
}
