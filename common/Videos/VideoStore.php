<?php
namespace Useeng\Common\Videos\VideoStore;

use Aws\S3\S3Client;
use Phalcon\Config\Adapter\Ini as IniConfig;

include_once USEENG_ROOT . "aws/vendor/autoload.php";

class Videos {
	protected static $_config;
	protected $_s3_client;

	function __construct() {
		$this->_s3_client = S3Client::factory([
			'key' => 'AKIAJIN74IF73W2GNZYA',
			'secret' => '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',
		]);

		$this->_config = new IniConfig(USEENG_ROOT . "config.ini.php");
	}

	function save($id, $video_stream) {
		$filename = $this->_config->media_name["feed_video"] . $this->_config->media_type['video'];
		$path = $this->_config->images['temp_path'] . $this->_config->feed['feed_path'] . "{$id}/";
		$upload_path = $this->generateUploadPath($id);
		try {
			if (!is_dir($path)) {
				mkdir("{$path}", 0777, true);
			}
			$pathToFile = $path . $filename;

			$video_decode = base64_decode($video_stream);
			$fp = fopen($pathToFile, 'w');
			fwrite($fp, $video_decode);
			fclose($fp);

			$media = "useeng-static"; //self::getMedia($section, $params);
			$upload_path = $this->generateUploadPath($id);
			$uri = $this->_getAssetPath($upload_path);
			$ret = null;

			if (!empty($media) && !empty($uri)) {
				$ret = $this->_s3_client->putObject(array(
					'Bucket' => $media,
					'Key' => $uri,
					'Body' => fopen($pathToFile, 'r+'),
				));
			} else {
				return false;
			}

			$this->rrmdir($pathToFile);

			return $ret != null ? true : false;

		} catch (Exception $e) {
			return false;
		}
	}

	function saveWithURL($id, $pathToFile) {
		$media = "useeng-static"; //self::getMedia($section, $params);
		$upload_path = $this->generateUploadPath($id);
		$uri = $this->_getAssetPath($upload_path);
		$ret = null;

		if (!empty($media) && !empty($uri)) {
			$ret = $this->_s3_client->putObject(array(
				'Bucket' => $media,
				'Key' => $uri,
				'Body' => fopen($pathToFile, 'r+'),
			));
		} else {
			return false;
		}

		$this->rrmdir($pathToFile);

		return $ret != null ? true : false;
	}

	protected function generateUploadPath($id, $hash = null) {
		$dir = md5($id);

		//#6992
		if (!empty($hash)) {
			$id += "_" . $hash;
		}

		return $dir{0} . $dir{1} . '/' . $dir{2} . '/' . $id;
	}

	protected function _getAssetPath($upload_path) {
		$folder_path = $this->_config->videos['feed_path'];
		$filename = $this->_config->media_name['feed_video'] . $this->_config->media_type['video'];

		$ret = sprintf('%s/%s/%s', $folder_path, $upload_path, $filename);

		// Prevent double slashes as some asset stores (like S3) will try to create a blank folder.
		// Ex: some/path//to/file should become some/path/to/file
		$ret = str_replace('//', '/', $ret);

		return $ret;
	}

	protected function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);

			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir . "/" . $object) == "dir") {
						rrmdir($dir . "/" . $object);
					} else {
						unlink($dir . "/" . $object);
					}

				}
			}

			reset($objects);
			rmdir($dir);
		}
	}

}
