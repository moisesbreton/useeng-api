<?php
ini_set('display_errors', 'On');

use Phalcon\Config\Adapter\Ini as IniConfig;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\DI\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\Micro;

DEFINE('USEENG_ROOT', __DIR__ . "/");

//Configuration
$config = new IniConfig("config.ini.php");

$di = new FactoryDefault();

//Set Configuration
$di->set('config', $config);

//Set up the database service
$di->set('db', function () use ($config) {
	return new MysqlAdapter([
		"host" => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
		"dbname" => $config->database->dbname,
	]);
});

//Create and bind the DI to the application
$app = new Micro($di);

// Use Loader() to autoload our model
$loader = new Loader();

$loader->registerDirs([
	__DIR__ . $app->config->useeng->models,
	__DIR__ . $app->config->useeng->controllers,
	__DIR__ . $app->config->useeng->common,
])->register();

$loader->registerNamespaces(
	[
		'Useeng\Common\Images' => __DIR__ . "/common/images/",
		'Useeng\Common\Videos' => __DIR__ . "/common/videos/",
	]
);

//User Controller
$userController = new UsersController();

//Log In User
$app->get('/users/login/{email}/{password}', [$userController, 'login']);

//Get User info
$app->get('/users/{id:[0-9]+}', [$userController, 'getUserInfo']);

//Adds a new user
$app->post('/users', [$userController, 'createUser']);

//Updates user based on primary key
$app->put('/users/{user_id:[0-9]+}', [$userController, 'updateUser']);

//Deletes user based on primary key
$app->delete('/users/{id:[0-9]+}', [$userController, 'deleteUser']);

//Log in or Register new User using Facebook
$app->post('/users/fblogin', [$userController, 'loginOrRegisterFacebook']);

//Feeds Controller
$feedController = new FeedsController();

//Create feed
$app->post('/feeds', [$feedController, 'createFeed']);

//Get feeds
$app->get('/feeds/{user_name}/{condition}/{page_num}', [$feedController, 'getAllFeeds']);

//Get Feeds for user
$app->get('/feeds/{user_name}/{page_num}', [$feedController, 'getUsersFeeds']);

//Get feed
$app->get('/feeds/{feed_id:[0-9]+}', [$feedController, 'getFeed']);

//Flag feed
$app->get('/feeds/{user_id:[0-9]+}/{feed_id:[0-9]+}', [$feedController, 'flagFeed']);

//Delete feed
$app->get('/feeds/{session_id}/{user_id:[0-9]+}/{feed_id:[0-9]+}', [$feedController, 'deleteFeed']);

//Categories Controller
$categoryController = new CategoriesController();

//Get Music/Category List
$app->get('/categories/getall/{parent_id}', [$categoryController, 'getAllCategories']);

//Migrator Controller
//$migratorController = new DataMigratorController();

//Migrating all the assets
//$app->get('/migrate/allassets', [$migratorController, 'migrateAllAssets']);

$app->handle();
