CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `label` varchar(256) NOT NULL DEFAULT '',
  `icon_image` varchar(256) NOT NULL DEFAULT '',
  `file_path` varchar(256) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;