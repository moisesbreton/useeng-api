CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL DEFAULT '',
  `email` varchar(256) NOT NULL DEFAULT '',
  `session_id` varchar(256) DEFAULT '',
  `has_photo` tinyint(4) NOT NULL DEFAULT '0',
  `upload_path` varchar(256) NOT NULL DEFAULT '',
  `hash` varchar(256) DEFAULT NULL,
  `facebook_uid` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
