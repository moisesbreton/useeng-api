CREATE TABLE `feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `caption` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `upload_path` varchar(256) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;