; <?php exit( 'This is not a config file ;-)' ); ?>

[database]
adapter  = Mysql
host = jebrtdb.c5a0lraasjru.us-east-1.rds.amazonaws.com
username = mbreton
password = "Breton&2010"
dbname = useeng

[useeng]
controllers = "/controllers/"
models = "/models/"
common = "/common/"

[media]
media_url = "http://useeng-static.s3.amazonaws.com/"

[images]
profile_path_placeholder = "images/static/user_placeholder.jpg"
profile_path = "images/profile/"
feed_path = "images/feeds/"
temp_path = "uploads/"

[videos]
feed_path = "feeds/videos/"

[category]
path = "uploads/category/"

[media_name]
feed_image = "img"
feed_video = "vid"
profile_image = "user_profile"
cat_icon = "_icon"
cat_music = "_music"

[media_type]
image = ".jpg"
video = ".mp4"
icon = ".png"
music = ".mp3"

[result]
number_of_results = 10
