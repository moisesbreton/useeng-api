<?php
use Phalcon\Mvc\Controller;

include_once USEENG_ROOT . "common/Images/PhotoStore.php";

class CategoriesController extends Controller {

	public function index() {
		//...
	}

	public function createCategory() {

	}

	public function getAllCategories($parent_id = 0) {
		$response = new Phalcon\Http\Response();

		$categories = Categories::find(
			[
				'conditions' => 'parent_id = :parent_id:',
				'bind' => [
					'parent_id' => $parent_id,
				],
				'order' => 'date_created DESC',
			]
		);

		if (count($categories) > 0) {
			$results = [];
			$results = ["total" => count($categories)];
			foreach ($categories as $category) {
				$results["categories"][] = [
					"id" => $category->id,
					"label" => $category->label,
					"icon_image" => isset($category->icon_path) ? $category->icon_path : '',
					"file_path" => isset($category->file_path) ? $category->file_path : '',
				];
			}

			$response->setJsonContent($results);
		} else {
			$response->setJsonContent(["total" => 0]);
		}

		return $response;
	}

}
