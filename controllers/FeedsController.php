<?php
use Phalcon\Mvc\Controller;
use Useeng\Common\Images\PhotoStore\Images as Images;
use Useeng\Common\Videos\VideoStore\Videos as Videos;

include_once USEENG_ROOT . "common/Images/PhotoStore.php";
include_once USEENG_ROOT . "common/Videos/VideoStore.php";

class FeedsController extends Controller {

	public function index() {
		//...
	}

	public function createFeed() {
		$raw_feed = $this->request->getJsonRawBody();
		$response = new Phalcon\Http\Response();

		$feed = new Feeds();
		$feed->user_id = $raw_feed->user_id;
		$feed->caption = $raw_feed->caption;
		$feed->category_id = !empty($raw_feed->category_id) ? $raw_feed->category_id : 0;

		$success = $feed->save();

		if ($success) {
			$error = false;
			$error_message = '';
			if ($raw_feed->image_data) {
				$video_image = new Images(base64_decode($raw_feed->image_data));
			} else {
				$error = true;
				$error_message = "There is not image in the video added!";
			}

			if ($raw_feed->video_data) {
				$video_data = $raw_feed->video_data;
			} else {
				$error = true;
				$error_message = "There is not video to upload!";
			}

			if (!$error) {
				if (!empty($video_image)) {
					$upload_path = $video_image->saveJPG($feed->id, "feed", 85);

					if ($upload_path) {
						$feed->upload_path = $upload_path;
						$feed->save();
					} else {
						$feed->delete();
						$response->setJsonContent([
							'success' => 'false',
							'message' => 'Feed image failed to be upload!',
						]);

						return $response;
					}
				} else {
					$feed->delete();
					$response->setJsonContent([
						'success' => 'false',
						'message' => 'Feed image failed to be upload!',
					]);

					return $response;
				}

				$video_handler = new Videos();
				$success = $video_handler->save($feed->id, $video_data);

				if (!$success) {
					$feed->delete();
					$response->setJsonContent([
						'success' => 'false',
						'message' => 'Video failed to be upload!',
					]);

					return $response;
				}

				//Video Feed Image
				$video_feed_image = $feed->getFeedPicturePath($this->config);

				$response->setJsonContent([
					'success' => 'true',
					'message' => 'Feed was created successfully!',
					'feed' => [
						"id" => $feed->id,
						"feed_image" => $video_feed_image,
						"caption" => $feed->caption,
					],
				]);

			} else {
				$feed->delete();
				$response->setJsonContent([
					'success' => 'false',
					'message' => $error_message,
				]);

				return $response;
			}

		} else {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'Failing creating the feed!',
			]);
		}

		return $response;
	}

	public function getFeed($feed_id) {
		$response = new Phalcon\Http\Response();

		$feed = Feeds::find([
			'conditions' => 'id = :id: AND status = 1',
			'bind' => ['id' => $feed_id],
			"order" => "date_created DESC",
		])->getFirst();

		if ($feed) {
			$result["success"] = 'true';

			$media_url = $this->config->media["media_url"];

			//User Profile picture
			$profile_image = $feed->users->getProfilePicturePath($this->config);

			//Video Feed source
			$video_feed_source = $feed->getFeedVideoSource($this->config);

			//Video Feed Image
			$video_feed_image = $feed->getFeedPicturePath($this->config);

			$results["feed"][] = [
				"username" => $feed->users->name,
				"profile_image" => $profile_image,
				"feed_image" => $video_feed_image,
				"feed_video" => $video_feed_source,
				"caption" => $feed->caption != null ? $feed->caption : '',
				"category_id" => $feed->categories->id,
				"category" => $feed->categories->label,
				"user_id" => $feed->users->id,
			];

			$response->setJsonContent($results);
		} else {
			$response->setJsonContent([
				"sucess" => 'false',
				"message" => 'This feeds was not found!',
			]);
		}

		return $response;
	}

	public function getUsersFeeds($user_name, $page_num) {
		$response = new Phalcon\Http\Response();

		if (empty($page_num) || $page_num < 1) {
			$page_num = 1;
		}
		$page_num -= 1;

		$num_of_results = $this->config->result["number_of_results"];

		$offset = $page_num * $num_of_results;

		$user = Users::getUserByUserName($user_name);

		$feeds = Feeds::find([
			'conditions' => 'user_id = :user_id: AND status = 1',
			'bind' => ['user_id' => $user_id],
			"order" => "date_created DESC",
			"limit" => [
				"number" => $num_of_results, "offset" => $offset,
			],
		]);

		if (count($feeds) > 0) {
			$results = [];
			$results = ["total" => count($feeds)];
			foreach ($feeds as $feed) {
				$media_url = $this->config->media["media_url"];

				//User Profile picture
				$profile_image = $feed->users->getProfilePicturePath($this->config);

				//Video Feed source
				$video_feed_source = $feed->getFeedVideoSource($this->config);

				//Video Feed Image
				$video_feed_image = $feed->getFeedPicturePath($this->config);

				$results["feeds"][] = [
					"username" => $feed->users->name,
					"profile_image" => $profile_image,
					"feed_image" => $video_feed_image,
					"feed_video" => $video_feed_source,
					"caption" => $feed->caption != null ? $feed->caption : '',
					"category_id" => $feed->categories->id,
					"category" => $feed->categories->label,
					"user_id" => $feed->users->id,
				];
			}

			$response->setJsonContent($results);
		} else {
			$response->setJsonContent(["total" => 0]);
		}

		return $response;
	}

	public function getAllFeeds($user_name, $option, $page_num) {
		$response = new Phalcon\Http\Response();

		if (empty($page_num) || $page_num < 1) {
			$page_num = 1;
		}
		$page_num -= 1;

		$num_of_results = $this->config->result["number_of_results"];

		$offset = $page_num * $num_of_results;

		if ($option == "getall") {
			$feeds = Feeds::find([
				'conditions' => 'status = 1',
				"order" => "date_created DESC",
				"limit" => [
					"number" => $num_of_results, "offset" => $offset,
				],
			]);
		}

		// if ($option == "followers") Do something when we add the followers
		if (count($feeds) > 0) {
			$results = [];
			$results = ["total" => count($feeds)];
			foreach ($feeds as $feed) {
				$media_url = $this->config->media["media_url"];

				//User Profile picture
				$profile_image = $feed->users->getProfilePicturePath($this->config);

				//Video Feed source
				$video_feed_source = $feed->getFeedVideoSource($this->config);

				//Video Feed Image
				$video_feed_image = $feed->getFeedPicturePath($this->config);

				$results["feeds"][] = [
					"username" => $feed->users->name,
					"profile_image" => $profile_image,
					"feed_image" => $video_feed_image,
					"feed_video" => $video_feed_source,
					"caption" => $feed->caption != null ? $feed->caption : '',
					"feed_id" => $feed->id,
					"category_id" => $feed->categories->id,
					"category" => $feed->categories->label,
					"user_id" => $feed->users->id,
				];
			}

			$response->setJsonContent($results);
		} else {
			$response->setJsonContent(["total" => 0]);
		}

		return $response;
	}

	public function flagFeed($user_id, $feed_id) {
		$response = new Phalcon\Http\Response();

		$flag_feed = new FlagFeeds();
		$flag_feed->user_id = $user_id;
		$flag_feed->feed_id = $feed_id;

		$sucess = $flag_feed->save();

		if ($sucess) {
			$response->setJsonContent([
				'success' => 'true',
			]);
		} else {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'Failing flagging the feed!',
			]);
		}

		return $response;
	}

	public function deleteFeed($session_id, $user_id, $feed_id) {
		$response = new Phalcon\Http\Response();

		//Validate user
		$feed = Feeds::find([
			'conditions' => 'user_id = :user_id: AND id = :feed_id:',
			'bind' => ['user_id' => $user_id, 'feed_id' => $feed_id],
		])->getFirst();

		if ($feed !== null) {
			if ($feed->users->hash == $session_id) {
				$feed->status = 0;

				$success = $feed->save();

				if ($success) {
					$response->setJsonContent([
						'success' => 'true',
					]);
				} else {
					$response->setJsonContent([
						'success' => 'false',
						'message' => 'There was an error deleting the feed!',
					]);
				}
			} else {
				$response->setJsonContent([
					'success' => 'false',
					'message' => 'Failing validating the user!',
				]);
			}
		} else {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'This is not your feed!',
			]);
		}

		return $response;
	}
}
