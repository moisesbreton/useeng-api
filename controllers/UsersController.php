<?php
use Phalcon\Mvc\Controller;
use Useeng\Common\Images\PhotoStore\Images as Images;

include_once USEENG_ROOT . "common/Images/PhotoStore.php";

class UsersController extends Controller {

	public function index() {
		//...
	}

	public function login($email, $password) {
		$password = md5($password);
		$params = [
			'conditions' => 'email = :email: AND password = :password:',
			'bind' => ['email' => $email, 'password' => $password],
		];

		$user = Users::find($params)->getFirst();

		$response = new Phalcon\Http\Response();

		if ($user != false) {

			$session_id = md5(rand());
			$user->hash = $session_id;
			$update_status = $user->save();

			$response->setJsonContent([
				'success' => 'true',
				'data' => [
					'id' => $user->id,
					'name' => $user->name,
					'email' => $user->email,
					'session_id' => $update_status ? $session_id : $user->hash,
					'has_photo' => $user->has_photo,
					'upload_path' => $user->getProfilePicturePath($this->config),
				],
			]);
		} else {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'Invalid Username and/or Password',
			]);
		}

		return $response;
	}

	public function loginOrRegisterFacebook() {
		$raw_user = $this->request->getJsonRawBody();
		$response = new Phalcon\Http\Response();

		//Check if the user already exist with the Facebook UID or Email
		$params = [
			'conditions' => 'facebook_uid = :facebookUID: OR email = :email:',
			'bind' => ['email' => $raw_user->email, 'facebookUID' => $raw_user->facebookUID],
		];

		$user = Users::find($params)->getFirst();

		//User exist?
		if ($user) {
			if ($user->facebook_uid == null) {
				$user->facebook_uid = $raw_user->facebookUID;
				$user->save();
			}

			$response->setJsonContent([
				'success' => 'true',
				'data' => [
					'id' => $user->id,
					'name' => $user->name,
					'email' => $user->email,
					'session_id' => $user->hash,
					'has_photo' => $user->has_photo,
					'upload_path' => $user->getProfilePicturePath($this->config),
				],
			]);
		} else {
			$user = new Users();
			$user->facebook_uid = $raw_user->facebookUID;
			$user->name = $raw_user->name;
			$user->email = $raw_user->email;
			$user->hash = md5(rand());

			if (!empty($raw_user->data)) {
				$profile_pic = new Images(base64_decode($raw_user->data));
			}

			$status = $user->save();

			if ($status) {
				if (!empty($profile_pic)) {
					$upload_path = $profile_pic->saveJPG($user->id, "profile", 85);

					if ($upload_path) {
						$user->has_photo = 1;
						$user->upload_path = $upload_path;
						$user->save();
					}
				}

				if ($user != false) {
					$response->setJsonContent([
						'success' => 'true',
						'data' => [
							'id' => $user->id,
							'name' => $user->name,
							'email' => $user->email,
							'session_id' => $user->hash,
							'has_photo' => $user->has_photo,
							'upload_path' => $user->getProfilePicturePath($this->config),
						],
					]);
				} else {
					$response->setJsonContent([
						'success' => 'false',
						'message' => 'User could not be save!',
					]);
				}
			} else {
				$response->setJsonContent([
					'success' => 'false',
					'message' => 'Failing creating the user!',
				]);
			}
		}

		return $response;
	}

	public function createUser() {

		$raw_user = $this->request->getJsonRawBody();
		$response = new Phalcon\Http\Response();

		//Validation user
		$params = [
			'conditions' => 'email = :email: OR name = :name:',
			'bind' => ['email' => $raw_user->email, 'name' => $raw_user->name],
		];

		if (Users::find($params)->getFirst()) {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'The user already exist!',
			]);

			return $response;
		}

		$user = new Users();
		$user->password = md5($raw_user->password);
		$user->name = $raw_user->name;
		$user->email = $raw_user->email;
		$user->hash = md5(rand());

		if (!empty($raw_user->data)) {
			$profile_pic = new Images(base64_decode($raw_user->data));
		}

		$status = $user->save();

		if ($status) {
			if (!empty($profile_pic)) {
				$upload_path = $profile_pic->saveJPG($user->id, "profile", 85);

				if ($upload_path) {
					$user->has_photo = 1;
					$user->upload_path = $upload_path;
					$user->save();
				}
			}

			if ($user != false) {
				$response->setJsonContent([
					'success' => 'true',
					'data' => [
						'id' => $user->id,
						'name' => $user->name,
						'email' => $user->email,
						'session_id' => $user->hash,
						'has_photo' => $user->has_photo,
						'upload_path' => $user->getProfilePicturePath($this->config),
					],
				]);
			} else {
				$response->setJsonContent([
					'success' => 'false',
					'message' => 'User could not be save!',
				]);
			}
		} else {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'Failing creating the user!',
			]);
		}

		return $response;
	}

	public function updateUser($user_id) {
		$response = new Phalcon\Http\Response();
		$raw_user = $this->request->getJsonRawBody();

		$edit_user = Users::findFirst($user_id);
		$user_edited = false;

		if ($edit_user) {
			if ($edit_user->name != $raw_user->name) {
				//Validation user
				$params = [
					'conditions' => ' name = :name:',
					'bind' => ['name' => $raw_user->name],
				];

				if (Users::find($params)->getFirst()) {
					$response->setJsonContent([
						'success' => 'false',
						'message' => 'The name already exist!',
					]);

					return $response;
				}

				$edit_user->name = $raw_user->name;
				$user_edited = true;
			}

			if ($edit_user->email != $raw_user->email) {
				//Validation user
				$params = [
					'conditions' => ' email = :email:',
					'bind' => ['email' => $raw_user->email],
				];

				if (Users::find($params)->getFirst()) {
					$response->setJsonContent([
						'success' => 'false',
						'message' => 'The email already exist!',
					]);

					return $response;
				}

				$edit_user->email = $raw_user->email;
				$user_edited = true;
			}

			if (!empty($raw_user->data)) {
				$profile_pic = new Images(base64_decode($raw_user->data));

				$upload_path = $profile_pic->saveJPG($edit_user->id, "profile", 85);

				if ($upload_path) {
					$edit_user->has_photo = 1;
					$edit_user->upload_path = $upload_path;
					$user_edited = true;
				}
			}

			if ($user_edited) {
				$edit_user->date_updated = date("Y/m/d H:i:s");
				
				if($edit_user->save()) {
					$response->setJsonContent([
                                 	       'success' => 'true',
                                        	'message' => 'User was update successfully!',
                                        	'data' => [
                                                	'id' => $edit_user->id,
                                                	'name' => $edit_user->name,
                                                	'email' => $edit_user->email,
                                                	'session_id' => $edit_user->hash,
                                                	'has_photo' => $edit_user->has_photo,
                                                	'upload_path' => $edit_user->getProfilePicturePath($this->config),
                                        	],
                                	]);
				} else {

					$errorMessage = "";
					foreach ($edit_user->getMessages() as $message) {
     					   $errorMessage .= $message. "\n";
    					}
					$response->setJsonContent([
						'success' => 'fail',
						'message' => $errorMessage,
					]);
				}
			} else {
				$response->setJsonContent([
					'success' => 'fail',
					'message' => 'There was nothing to updated!',
				]);
			}

		} else {
			$response->setJsonContent([
				'success' => 'false',
				'message' => 'User does not exist!',
			]);
		}

		return $response;
	}

	public function deleteUser($user_id) {
		//....
	}
}
